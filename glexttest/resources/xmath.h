/* math.h extesion with vector, matrixes and transformations; 
 Planned: non-linear interpolations;
 Depends on math.h;
 Adds a new vector type;
 Adds a new matrix type;

 Lots of vector operators:
 ==,!= comparison
 +,-,+=,-= addition subtraction
 +,-,*,/ add sub or multiply with floats
 * dot product
 ^ cross product
 ~ vector size
 ! converts to unit vector

 Functions: 
 > vsize, vnorm, vnormal, vdist, vdistance, vcosa, vcosangle, vangl, vangle; 
 vnorm = !, vsize = ~, vdist = ~(a-b);
 > lerp(A,B,f); linearly interpolates 2 floats/vectors with a float as 3rd parameter [0.0-1.0]

 You can get the normal of a polygon with: !((A-B)^(C-B))

 Matrixes type matrix
 Functions:
 m_identity,m_translate,m_scale,m_rotate,m_mul,m_transform
 Operators:
 matrix * matrix multiplication 
 vector * matrix transformation
*/

#ifndef _INC_XMATH
#define _INC_XMATH
#define _USE_MATH_DEFINES
#include <math.h>
#define xmfloatt float

typedef struct 
{
	xmfloatt x,y,z;
}vector;

typedef struct
{
	xmfloatt m[4][4];
}matrix;

#define vnorm vnormal
#define vdist vdistance
#define vcosa vcosangle
#define vangl vangle


inline xmfloatt todeg(xmfloatt a) { return a*57.295779513082320876798154814105f; }
inline xmfloatt torad(xmfloatt a) { return a*0.017453292519943295769236907684886f; }


bool operator == (vector a, vector b)	;
bool operator != (vector a, vector b)	;
vector operator - (vector a)			;


inline void operator += (vector &a,vector &b)	{ a.x+=b.x; a.y+=b.y; a.z+=b.z; }
inline void operator -= (vector &a,vector &b)	{ a.x-=b.x; a.y-=b.y; a.z-=b.z; }
inline vector operator + (vector a, vector &b)	{ a+=b; return a; }
inline vector operator - (vector a, vector &b)	{ a-=b; return a; }
inline xmfloatt operator * (vector &a, vector &b)	{ return a.x*b.x+a.y*b.y+a.z*b.z; }
inline vector operator ^ (vector &a, vector &b)	{ vector c; c.x=a.y*b.z-b.y*a.z; c.y=a.z*b.x-b.z*a.x; c.z=a.x*b.y-b.x*a.y; return c;}

inline void operator += (vector &a,xmfloatt b)	{ a.x+=b; a.y+=b; a.z+=b; }
inline void operator -= (vector &a,xmfloatt b)	{ a.x-=b; a.y-=b; a.z-=b; }
inline void operator *= (vector &a,xmfloatt b)	{ a.x*=b; a.y*=b; a.z*=b; }
inline void operator /= (vector &a,xmfloatt b)	{ a.x/=b; a.y/=b; a.z/=b; }
inline vector operator + (vector a, xmfloatt b)	{ a+=b; return a; }
inline vector operator + (xmfloatt b, vector a)	{ a+=b; return a; }
inline vector operator - (vector a, xmfloatt b)	{ a-=b; return a; }
inline vector operator * (vector a, xmfloatt b)	{ a*=b; return a; }
inline vector operator * (xmfloatt b, vector a)	{ a*=b; return a; }
inline vector operator / (vector a, xmfloatt b)	{ a/=b; return a; }

inline xmfloatt vsize (vector &a)					{ return sqrt(a.x*a.x+a.y*a.y+a.z*a.z); }
inline xmfloatt operator ~(vector &a)				{ return vsize(a); }
inline xmfloatt vdistance (vector &a, vector &b)	{ return ~(a-b); } 
inline vector vnormal (vector &a)				{ return a/(~a); } 
inline vector operator !(vector &a)				{ return vnorm(a); }
inline xmfloatt vcosangle (vector &a, vector &b ) { return !a*!b; }
inline xmfloatt vangle (vector &a, vector &b )    { return acos(vcosa(a,b)); }
inline vector v(xmfloatt a,xmfloatt b, xmfloatt c)	{ vector d; d.x=a; d.y=b; d.z=c; return d; }
inline vector v(xmfloatt *a)						{vector b; b.x=a[0]; b.y=a[1]; b.z=a[2];}
inline vector lerp(vector &a, vector &b, xmfloatt c) {return a+c*(b-a); }
inline xmfloatt lerp(xmfloatt &a, xmfloatt &b, xmfloatt c) {return a+c*(b-a); }

void m_identity(matrix  &a);
matrix m_identity();

void m_mul(matrix &a,matrix &b, matrix &c);
matrix operator * (matrix &a,matrix &b);
void operator *= (matrix &a,matrix &b);

void m_translate(matrix &a, xmfloatt x, xmfloatt y, xmfloatt z);
void m_translate(matrix &a, vector &b);
matrix m_translate(xmfloatt x, xmfloatt y, xmfloatt z);
matrix m_translate(vector &b);

void m_scale(matrix &a, xmfloatt s);
void m_scale(matrix &a, xmfloatt x, xmfloatt y, xmfloatt z);
void m_scale(matrix &a, vector &b);
matrix m_scale(xmfloatt s);
matrix m_scale(xmfloatt x,xmfloatt y, xmfloatt z);
matrix m_scale(vector &b);

void m_rotate_x(matrix &a, xmfloatt q);
void m_rotate_y(matrix &a, xmfloatt q);
void m_rotate_z(matrix &a, xmfloatt q);
matrix m_rotate_x(xmfloatt q);
matrix m_rotate_y(xmfloatt q);
matrix m_rotate_z(xmfloatt q);

void m_rotate(matrix &a,xmfloatt q,xmfloatt x, xmfloatt y, xmfloatt z);
void m_rotate(matrix &a,xmfloatt q, vector &b);
matrix m_rotate(xmfloatt q, xmfloatt x, xmfloatt y, xmfloatt z);
matrix m_rotate(float q, vector &b);

void m_transform(vector &a, matrix &b);
vector operator *(vector &c, matrix &b);
void operator *= (vector &c, matrix &b);

#endif
