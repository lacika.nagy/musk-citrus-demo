#pragma once
float linear(const float t, const float p0, const float p1);
float smoothstep(const float t, const float p0, const float p1);
float smootherstep(const float t, const float p0, const float p1);
float cubic_spline(const float t, const float p0, const float p1, const float p2, const float p3);
float catmull_rom(const float t, const float p0, const float p1, const float p2, const float p3);
float bezier(const float t, const float p0, const float c0, const float c1, const float p1);
float hermite(const float t, const float p0, const float p1, const float ta0, const float ta1);
float sample_linear(const float t, const float* data,const int nr);
float sample_cubic(const float t, const float* data, const int nr);
float sample_catmull(const float t, const float* data, const int nr);
float sample_bezier(const float t, const float* pos, const int nr, const float* cp_in, const float* cp_out);
float sample_bezier_dcp(const float t, const float* pos, const int nr, const float* cp_in, const float* cp_out);
float sample_hermite(const float t, const float* pos, const int nr, const float* tan_in, const float* tan_out);
float sample_inverse_linear(const float t, const float* data, const int nr);
float sample_custom(const float t, const float* pos, const int nr);
float sample_inverse_catmull(const float t, const float* data, const int nr);
float sample_inverse_custom(const float t, const float* data, const int nr);