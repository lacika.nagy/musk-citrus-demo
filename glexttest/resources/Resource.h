#pragma once

//#include "ResourceTypes.h"

class Resource
{
public:
	virtual int Load();
	virtual int Unload();
	int Size;
	void* Data;
	char* Name;
	char* Path;
	Resource();	
	~Resource();
};

class ResourceTable
{
public:
	Resource* Find(char*);
	Resource* Fetch(char*);//find res, load if needed,
	void* Get(char*); //same as fetch, but returns different object types

	int Error;

	Resource** List; //list of pointers
	int ListCapacity;
	int ListSize;
	ResourceTable();
	~ResourceTable();
};

extern ResourceTable GRT;
