#undef UNICODE
#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "myext.h"
#include "ResourceTypes.h"
#include <string.h>
#include "Curves.h"
#include <assert.h>
#include <stdio.h>
#include "bass.h"

void MeshRender(const mesh* mm)
{
	if (mm == 0) return;
	int* tp = mm->triangle_list;

	glBegin(GL_TRIANGLES);
	for (int v = 0; v<mm->triangle_count*3; v++)
	{
		//{v0 n0 uv0 v1 n1 uv1 v2 n2 uv2}
		int vi,ni,ui;
		vi = *(tp++);
		ni = *(tp++);
		ui = *(tp++);
		glTexCoord2f(*(mm->uv_list + (ui*2)), *(mm->uv_list + (ui*2+1)));
		glNormal3f(*(mm->normal_list + (ni*3)),*(mm->normal_list + (ni*3+1)),*(mm->normal_list + (ni*3+2)));
		glVertex3f(*(mm->vertex_list + (vi*3)),*(mm->vertex_list + (vi*3+1)),*(mm->vertex_list + (vi*3+2)));
	}
	glEnd();
}
void MeshDisplay(const mesh* mm)
{
	glCallList(mm->glid);
}
void SceneObjectApplyTransform(const scene_object* cso)
{
	if (cso == 0) return;
	glTranslatef(cso->x,cso->y,cso->z);
	if (cso->joz!=0.0) glRotatef(cso->joz,0.0,0.0,1.0);
	if (cso->joy!=0.0) glRotatef(cso->joy,0.0,1.0,0.0);
	if (cso->jox!=0.0) glRotatef(cso->jox,1.0,0.0,0.0);
	if (cso->zr!=0.0) glRotatef(cso->zr,0.0,0.0,1.0);
	if (cso->yr!=0.0) glRotatef(cso->yr,0.0,1.0,0.0);
	if (cso->xr!=0.0) glRotatef(cso->xr,1.0,0.0,0.0);
	
	glScalef(cso->xs,cso->ys,cso->zs);
	if (cso->xs != 1.0f || cso->ys != 1.0f || cso->zs != 1.0f)
	{
		glEnable(GL_NORMALIZE);
	}
	else
	{
		glDisable(GL_NORMALIZE);
	}
}
void SceneObjectApplyAnim(scene_object* cso, const float t)
{
	if (cso == 0) return;
	anim* aa;
	aa=cso->anim_group[0];
	if (aa) cso->x=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);
	aa=cso->anim_group[1];
	if (aa) cso->y=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);
	aa=cso->anim_group[2];
	if (aa) cso->z=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);
	aa=cso->anim_group[3];
	if (aa) cso->xr=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);
	aa=cso->anim_group[4];
	if (aa) cso->yr=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);
	aa=cso->anim_group[5];
	if (aa) cso->zr=sample_bezier_dcp(sample_inverse_catmull(t,aa->input,aa->count),aa->output,aa->count,aa->itangent,aa->otangent);	
}
void glBindTexture(RTexture* x)
{
	if (x != NULL)
	{
		glBindTexture(GL_TEXTURE_2D,x->glid);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D,NULL);
	}
}
void glBindProgram(RShadingProgram* x)
{
	if (x != NULL)
	{
		glUseProgram(x->glid);
	}
	else
	{
		glUseProgram(NULL);
	}
};

int RTexture::Load()
{
	if (this->gltex != 0)
	{
		delete this->gltex;
	}

	this->gltex = new gl::Texture(this->Path);
	this->glid = gltex->id;
	this->tex = gltex;

	return 0;
}

int RTexture::Unload()
{
	delete this->gltex;
	this->tex = 0;
	this->gltex = 0;
	this->Data = 0;

	return 0;
}

RTexture::RTexture()
{
	glid=0;
	memset(this,0,sizeof(this));
}
RTexture::~RTexture()
{
	if (gltex != 0)
	{
		this->Unload();
	}
}

const GLchar *stupidshit[1];

RShadingProgram::RShadingProgram()
{
	memset(this,0,sizeof(this));
}

int RShadingProgram::Load()
{
	if (glprg == 0) delete glprg;
	glfs = new gl::FragmentShader(this->fs_path);
	glvs = new gl::VertexShader(this->vs_path);
	this->fssource = glfs->source;
	this->vssource = glvs->source;
	this->fsid = glfs->id;
	this->vsid = glvs->id;
	glprg = new gl::Program(glfs,glvs);
	this->glid = glprg->id;

	return 0;
}

RShader::RShader()
{
	glshader = 0;
	type = 0;
}

int RShader::Load()
{
	char *c = this->Path;
	while (c!=0) c++;
	unsigned u = *((unsigned*)c-4);
	this->type = 0;
	switch (u)
	{
	case 'garf': // frag
		this->type = GL_FRAGMENT_SHADER;
		break;
	case 'trev': // vert
		this->type = GL_VERTEX_SHADER;
		break;
	}
	glshader = new gl::Shader(type,this->Path);
	return 0;
}

/*
class RColladaScene: public Resource
{
public:
	visual_scene* scene;
	scene_object* camera;
	scene_object* camera_target;

	scene_object* FindSceneObject(const char* name);
	virtual int Load();
};
*/

collada_data_table *cdt = 0;

int RColladaScene::Load()
{


	if (true)
	{
		local_cdt = (collada_data_table*) malloc(sizeof(collada_data_table));
		cdt = local_cdt;
		cdt->anim_capacity = 90; cdt->anim_count=0;
		cdt->anim_list = (anim*) malloc(cdt->anim_capacity*sizeof(anim));

		cdt->mesh_capacity = 30; cdt->mesh_count=0;
		cdt->mesh_list = (mesh*) malloc(cdt->mesh_capacity*sizeof(mesh));

		cdt->scene_object_capacity = 30; cdt->scene_object_count=0;
		cdt->scene_object_list = (scene_object*) malloc(cdt->scene_object_capacity*sizeof(scene_object));

		cdt->visual_scene_capacity = 10; cdt->visual_scene_count=0;
		cdt->visual_scene_list = (visual_scene*) malloc(cdt->anim_capacity*sizeof(visual_scene));
	}

	int osn = cdt->visual_scene_count;
	load_collada(cdt,this->Path);
	
	if (cdt->visual_scene_count > osn)
	{
		this->scene = cdt->visual_scene_list + osn;
	}
	 
	this->camera = this->FindSceneObject("CAMERA");
	this->camera_target = this->FindSceneObject("CAMERA_Target");
	
	return 0;
}

scene_object* RColladaScene::FindSceneObject(const char* soname)
{
	if (this->scene != 0)
	{
		
		for (int i=0; i<this->scene->scene_object_count; i++)
		{
			if (strcmp((*(this->scene->scene_object_plist+i))->name,soname)==0)
				return *(this->scene->scene_object_plist+i);
		}
		
	}
	return 0;
}
/*
class RAudioStream: public Resource
{
public:
	float Panning;
	float Volume;
	float BPS;
	void* Handle;

	float lst;

	RAudioStream()
	{
		Panning = 0.0;
		Volume = 1.0f;
		BPS = 1.0f;
		Handle = 0;
		lst = 0.0;
	}
	void Play();
	void Stop();

	float SyncToPlay(float t);

	virtual int Load();
};*/

int RAudioStream::Load()
{
	HSTREAM chan = BASS_StreamCreateFile(0,this->Path,0,0,0);
	bhandle = chan;
	if (!chan) MessageBox(0,"Audio stream failed!","Error",MB_OK);
	return 0;
}

void RAudioStream::Play()
{
	BASS_ChannelPlay(bhandle,1);
	BASS_ChannelSetAttribute(bhandle,BASS_ATTRIB_VOL,this->Volume);
	BASS_ChannelSetAttribute(bhandle,BASS_ATTRIB_PAN,this->Panning);
}

void RAudioStream::Stop()
{
	BASS_ChannelStop(bhandle);
}

float ffabs(float x)
{
	if (x<0.0f) return -x; 
	else return x;
}

float RAudioStream::SyncToPlay(float t)
{
	float bpos = BASS_ChannelBytes2Seconds(bhandle,BASS_ChannelGetPosition(bhandle,BASS_POS_BYTE));

	if (BASS_ChannelIsActive(bhandle) != BASS_ACTIVE_PLAYING)
	{
		Play();
		BASS_ChannelSetPosition(bhandle,BASS_ChannelSeconds2Bytes(bhandle,t),BASS_POS_BYTE);
	}
	else
	{
		if (ffabs(t-bpos)>0.1) BASS_ChannelSetPosition(bhandle,BASS_ChannelSeconds2Bytes(bhandle,t),BASS_POS_BYTE);
	}

	BASS_ChannelSetAttribute(bhandle,BASS_ATTRIB_VOL,this->Volume);
	BASS_ChannelSetAttribute(bhandle,BASS_ATTRIB_PAN,this->Panning);
	
	this->lst = t;
	return t*BPS;
}