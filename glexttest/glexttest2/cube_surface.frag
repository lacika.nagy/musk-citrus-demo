uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform sampler2D Texture2;

varying vec3 vertex_light_position;
varying vec3 vertex_normal;

float rand(float x)
{
    return cos(sin(cos(x*1311.321)*1333.421)*2124.512);
}

void main()
{
    vec4 acc = vec4(0.0 ,0.0 ,0.0 ,0.0 );
	//vec3 normal = normalize(vertex_normal + texture2D(texture,gl_TexCoord[0].xy).xyz-vec3(0.5,0.5,0.5));
	vec3 normal = 2.0*texture2D(Texture1,gl_TexCoord[0].xy).xyz-1.0;
	normal =  normalize(vertex_normal+normal);

	float diffuse = dot(vertex_light_position,normal);
	float specular = dot(reflect(-vertex_light_position,normal),normal);

	//if (specular < 0.0) specular = 0.0;
	//if (specular > 1.0) specular = 1.0;
	specular = pow(specular,2.0);
	specular = pow(specular,10.0);
	specular *=0.5;

	vec4 tex = texture2D(Texture0,gl_TexCoord[0].xy);
	vec4 detail = (texture2D(Texture2,gl_TexCoord[0].xy*4.0)-0.5)*2.0+0.5;//tex *= texture2D(Texture2,gl_TexCoord[0].xy*4.0);
	tex = tex*detail*2.0;

	diffuse+=0.2;
	//gl_FragColor = tex*0.4;
	//gl_FragColor = vec4(normal,1.0);
    gl_FragColor = gl_Color*diffuse*tex+ vec4(specular,specular,specular,1.0);
}
