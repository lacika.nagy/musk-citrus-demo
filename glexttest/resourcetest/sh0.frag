//uniform vec3      iResolution;     // viewport resolution (in pixels)
vec3 iResolution = vec3(800.0,600.0,0.0);
uniform float     iGlobalTime;     // shader playback time (in seconds)
uniform float     iChannelTime[4]; // channel playback time (in seconds)
//uniform vec4      iMouse;          // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;    // input channel. XX = 2D/Cube
uniform vec4      iDate;           // (year, month, day, time in seconds)

float rand(float q)
{
	return fract(sin(q)*43758.5453);
}

float noise(vec2 p)
{
	vec2 pm = mod(p,1.0);
	vec2 pd = p-pm;
	float v0=rand(pd.x+pd.y*41.0);
	float v1=rand(pd.x+1.0+pd.y*41.0);
	float v2=rand(pd.x+pd.y*41.0+41.0);
	float v3=rand(pd.x+pd.y*41.0+42.0);
	v0 = mix(v0,v1,pm.x);
	v2 = mix(v2,v3,pm.x);
	return mix(v0,v2,pm.y);
}

float dist(vec3 p)
{
	vec3 ps = p;
	
	
	p = ps;
	
	//p = mod(p,16.0);
	
	p.z+=iGlobalTime*2.0;
	float d1 = p.y+2.0;
	//d1-=texture2D(iChannel0,p.xz*0.0001)*1.0-20.5
	
	d1-=noise(p.xz*0.5);
	d1-=noise(p.xz*0.06125)*20.0;

	//d1-=texture2D(iChannel0,p.xz*0.01)*1.0-5.5;;
	//d1-=texture2D(iChannel0,p.xz*0.001)*20.0-5.5;;	

//	d1-=texture2D(iChannel0,p.xz*0.1)*0.1;
//	d1+=texture2D(iChannel0,(p.xy+d1)*0.001);
//	d1+=texture2D(iChannel0,(p.yz+d1)*0.001);
	
	return d1;
	
}

vec3 normal(vec3 p)
{
	float e=0.1;
	float d=dist(p);
	
	return normalize(vec3(dist(p+vec3(e,0,0))-d,dist(p+vec3(0,e,0))-d,dist(p+vec3(0,0,e))-d));
}

vec3 l =normalize(vec3(sin(iGlobalTime*0.41),sin(iGlobalTime*0.1)*0.3+0.3,cos(iGlobalTime*0.512)));

float shadow(vec3 p)
{
	float s = 0.0;
	for (int i=0; i<100; i++)
	{
		float d = dist(p);
		p+=l*(0.01+d*0.5);
		//float ss=clamp(0.0,d,1.0)*0.01;
		float ss=d; if (d>1.0) ss = 1.0; if (ss<0.0) ss=0.0; ss*=0.01;
		s+=ss;
		if (ss==0.00)
		{
			s=0.0;
			break;
		}
		if (p.y>150.0)
		{
			s+=float(99-i)*0.01; break;
		}
	}
	return pow(s,4.0);
}

vec3 sky(vec3 dir)
{
	//return vec3(0.4,0.6,0.9);
	dir.y = clamp(0.0,dir.y,1.0);
	float atmos = ((sin(l.y*3.14159+1.0))*pow((2.0-dir.y),4.0))*(dot(dir,l)*0.2+0.2);
	float atmos2 = ((l.y*0.5)*pow((2.0-dir.y),1.5));
	vec3 atmosc; atmosc.r = atmos*0.2; 
	atmosc.g=atmosc.r-0.5;atmosc.b=atmosc.g-0.3;
	atmosc = clamp(vec3(0.0),atmosc,vec3(2.0));
	
	
	vec3 atmos2c; atmos2c.b = atmos2*0.5; 
	atmos2c.g=atmos2c.b-0.2;atmos2c.r=atmos2c.g-0.2;
	atmos2c = clamp(vec3(0.0),atmos2c,vec3(2.0));
	vec3 final = atmosc*0.5+atmos2c;
	
	final += vec3(0.1,0.13,0.2);
	
	final = max(vec3(.0),final);
	return final;
}

vec3 sun(vec3 dir)
{
	float sun = dot(dir,l);
	sun+=1.0; sun*=0.5; sun= pow(sun,17.0);
	return vec3(sun);
}

void main(void)
{
	vec2 uv = gl_FragCoord.xy / iResolution.xy;
	
	//vec2 m = iMouse.xy / iResolution.xy - 0.5;
	vec3 pos = vec3(sin(iGlobalTime*0.13)*10.0,-1.0,cos(iGlobalTime*0.1));
	pos = vec3(pos.x,-dist(pos)+1.0,pos.z);
	vec3 dir = vec3((uv.x-0.5)*iResolution.x/iResolution.y,uv.y-0.5,1.0);
	dir.z = (1.0-length(dir)*0.5);
	//dir.xy += m;
	dir = normalize(dir);
	
	vec3 color = vec3(0);
	
	float t = iGlobalTime;
	
	float totald = 0.0;
	
	for (int i=0; i<200; i++)
	{
		//color = pos;
		float d = dist(pos);
		pos += dir * d*0.5;
		totald+=d;
		
		//color = sky(dir);
		if (pos.y>150.0 || i == 199)
		{
			
			color = sky(dir)+sun(dir);
			break;
		}
		if (d<totald*0.001)
		{
			//color.x=(float(i)/50.0);
			vec3 n = normal(pos);
			
			float diffuse = dot(normal(pos),l);
			float specular = dot(reflect(dir,n),l);
			if (diffuse<0.0) diffuse = 0.0;
			if (specular<0.0) specular = 0.0;
			specular = pow(specular,10.0);
			
			vec3 ambientc = (sky(dir)+sun(dir));
			
			color = vec3(0.4,0.5,0.3)*diffuse;
			
			//color = vec3(shadow(pos));
			color=
				vec3(shadow(pos))*(color+min(ambientc*0.4,vec3(0.4)))
				+ambientc*0.2;
			
			color = mix(color,ambientc+sun(dir),float(i)/200.0);
			
			break;
		}
		
	}
	
	
	
	
	gl_FragColor = vec4(color,1.0);
}