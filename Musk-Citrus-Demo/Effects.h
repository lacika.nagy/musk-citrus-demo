#pragma once

void RenderEffect0(int shaderProg, double time);
void RenderParticles(int shaderProg, double time);
void RenderEffect1(int shaderProg, double time);
void RenderArc(int shaderProg, double time);
void RenderCylinderField(int shaderProg, double time);
void RenderSemmi(int shaderProg, double time);
void RenderCity(int shaderProg, double time);