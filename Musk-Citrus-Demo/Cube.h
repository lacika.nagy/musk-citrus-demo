#pragma once
#include <glew.h>
#include <wglew.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <stdlib.h>
#include <vector>
#include <cmath>
#include "Vector.h"
#include "Matrix.h"

struct cubeVertex 
{
	float pos[4];
	float normal[4]; // the average normal
	short numFaces;  // number of faces shared by the vertex
	long colora;     // ambient colour - change to a colour structure
	long colord;     // diffuse color    - change to a colour structure
	long colors;     // specular colour  - change to a colour structure
};

class SolidCube 
{
protected:
	//Vector3f size;
	int numTriangles = 12;
	struct cubeVertex * vtx = NULL;
	int sphereVBO;   // Vertex handle that contains interleaved positions and colors
	int triangleVBO; // Triangle handle that contains triangle indices
	int * ind = NULL;
	int numInd;
public:
	Matrix4f modelMat = Matrix4f::identity();
	Matrix4f rotMat = Matrix4f::identity();
	Vector4f materialAmbient;
	Vector4f materialDiffuse;
	Vector4f materialSpecular;

	SolidCube()
	{
		materialAmbient = Vector4f(0, 0, 0, 0);
		materialDiffuse = Vector4f(0, 0, 0, 0);
		materialSpecular = Vector4f(0, 0, 0, 0);
		//this->size = size;

		int numVtx = 36;

		// allocate memory
		vtx = (struct cubeVertex *) malloc(sizeof(struct cubeVertex) * numVtx);
		if (vtx == NULL) {
			printf("Oops! An error occurred\n");

		}

		ind = (int *)malloc(sizeof(int) * numTriangles * 3);
		if (ind == NULL) {
			// error
			printf("Oops! An error occurred\n");
		}

		//    X                       Y                       Z                         W
		vtx[0].pos[0] = -1;		vtx[0].pos[1] = -1;		vtx[0].pos[2] = -1;		vtx[0].pos[3] = 1;
		vtx[1].pos[0] = -1;		vtx[1].pos[1] = -1;		vtx[1].pos[2] = 1;		vtx[1].pos[3] = 1;
		vtx[2].pos[0] = -1;		vtx[2].pos[1] = 1;		vtx[2].pos[2] = 1;		vtx[2].pos[3] = 1;

		vtx[3].pos[0] = 1;		vtx[3].pos[1] = 1;		vtx[3].pos[2] = -1;		vtx[3].pos[3] = 1;
		vtx[4].pos[0] = -1;		vtx[4].pos[1] = -1;		vtx[4].pos[2] = -1;		vtx[4].pos[3] = 1;
		vtx[5].pos[0] = -1;		vtx[5].pos[1] = 1;		vtx[5].pos[2] = -1;		vtx[5].pos[3] = 1;

		vtx[6].pos[0] = 1;		vtx[6].pos[1] = -1;		vtx[6].pos[2] = 1;		vtx[6].pos[3] = 1;
		vtx[7].pos[0] = -1;		vtx[7].pos[1] = -1;		vtx[7].pos[2] = -1;		vtx[7].pos[3] = 1;
		vtx[8].pos[0] = 1;		vtx[8].pos[1] = -1;		vtx[8].pos[2] = -1;		vtx[8].pos[3] = 1;

		vtx[9].pos[0] = 1;		vtx[9].pos[1] = 1;		vtx[9].pos[2] = -1;		vtx[9].pos[3] = 1;
		vtx[10].pos[0] = 1;		vtx[10].pos[1] = -1;	vtx[10].pos[2] = -1;	vtx[10].pos[3] = 1;
		vtx[11].pos[0] = -1;	vtx[11].pos[1] = -1;	vtx[11].pos[2] = -1;	vtx[11].pos[3] = 1;

		vtx[12].pos[0] = -1;	vtx[12].pos[1] = -1;	vtx[12].pos[2] = -1;	vtx[12].pos[3] = 1;
		vtx[13].pos[0] = -1;	vtx[13].pos[1] = 1;		vtx[13].pos[2] = 1;		vtx[13].pos[3] = 1;
		vtx[14].pos[0] = -1;	vtx[14].pos[1] = 1;		vtx[14].pos[2] = -1;	vtx[14].pos[3] = 1;

		vtx[15].pos[0] = 1;		vtx[15].pos[1] = -1;	vtx[15].pos[2] = 1;		vtx[15].pos[3] = 1;
		vtx[16].pos[0] = -1;	vtx[16].pos[1] = -1;	vtx[16].pos[2] = 1;		vtx[16].pos[3] = 1;
		vtx[17].pos[0] = -1;	vtx[17].pos[1] = -1;	vtx[17].pos[2] = -1;	vtx[17].pos[3] = 1;

		vtx[18].pos[0] = -1;	vtx[18].pos[1] = 1;		vtx[18].pos[2] = 1;		vtx[18].pos[3] = 1;
		vtx[19].pos[0] = -1;	vtx[19].pos[1] = -1;	vtx[19].pos[2] = 1;		vtx[19].pos[3] = 1;
		vtx[20].pos[0] = 1;		vtx[20].pos[1] = -1;	vtx[20].pos[2] = 1;		vtx[20].pos[3] = 1;

		vtx[21].pos[0] = 1;		vtx[21].pos[1] = 1;		vtx[21].pos[2] = 1;		vtx[21].pos[3] = 1;
		vtx[22].pos[0] = 1;		vtx[22].pos[1] = -1;	vtx[22].pos[2] = -1;	vtx[22].pos[3] = 1;
		vtx[23].pos[0] = 1;		vtx[23].pos[1] = 1;		vtx[23].pos[2] = -1;	vtx[23].pos[3] = 1;

		vtx[24].pos[0] = 1;		vtx[24].pos[1] = -1;	vtx[24].pos[2] = -1;	vtx[24].pos[3] = 1;
		vtx[25].pos[0] = 1;		vtx[25].pos[1] = 1;		vtx[25].pos[2] = 1;		vtx[25].pos[3] = 1;
		vtx[26].pos[0] = 1;		vtx[26].pos[1] = -1;	vtx[26].pos[2] = 1;		vtx[26].pos[3] = 1;

		vtx[27].pos[0] = 1;		vtx[27].pos[1] = 1;		vtx[27].pos[2] = 1;		vtx[27].pos[3] = 1;
		vtx[28].pos[0] = 1;		vtx[28].pos[1] = 1;		vtx[28].pos[2] = -1;	vtx[28].pos[3] = 1;
		vtx[29].pos[0] = -1;	vtx[29].pos[1] = 1;		vtx[29].pos[2] = -1;	vtx[29].pos[3] = 1;

		vtx[30].pos[0] = 1;		vtx[30].pos[1] = 1;		vtx[30].pos[2] = 1;		vtx[30].pos[3] = 1;
		vtx[31].pos[0] = -1;	vtx[31].pos[1] = 1;		vtx[31].pos[2] = -1;	vtx[31].pos[3] = 1;
		vtx[32].pos[0] = -1;	vtx[32].pos[1] = 1;		vtx[32].pos[2] = 1;		vtx[32].pos[3] = 1;

		vtx[33].pos[0] = 1;		vtx[33].pos[1] = 1;		vtx[33].pos[2] = 1;		vtx[33].pos[3] = 1;
		vtx[34].pos[0] = -1;	vtx[34].pos[1] = 1;		vtx[34].pos[2] = 1;		vtx[34].pos[3] = 1;
		vtx[35].pos[0] = 1;		vtx[35].pos[1] = -1;	vtx[35].pos[2] = 1;		vtx[35].pos[3] = 1;

		for (int i = 0; i < 36; i++)
		{
			ind[i] = i;
		}

		// *numVtx1 = numVtx;
		numInd = numTriangles * 3;

		glGenBuffers(1, (GLuint *)&sphereVBO);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(struct cubeVertex)*numVtx, vtx, GL_STATIC_DRAW);
		glGenBuffers(1, (GLuint *)&triangleVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * numInd, ind, GL_STATIC_DRAW);
	}

	~SolidCube()
	{
		std::cout << "Deleted sphere" << std::endl;
	}

	void rotateX(GLfloat angle, int degrees) 
	{
		Matrix4f tempRot = Matrix4f::rotateX(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	Vector4f getPosition() 
	{
		Vector4f pos = modelMat * Vector4f(0, 0, 0, 1.0);
		printf("x: %f, y: %f, z: %f\n", pos.x, pos.y, pos.z);
		return pos;
	}

	/**
	* Rotate the sphere around the y-axis.
	*
	* @param degrees Degrees to rotate it.
	*/
	void rotateY(GLfloat angle, int degrees) 
	{
		Matrix4f tempRot = Matrix4f::rotateY(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	void rotateZ(GLfloat angle, int degrees) 
	{
		Matrix4f tempRot = Matrix4f::rotateZ(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	void rotateVector(Vector3f v, GLfloat angle, int degrees) 
	{
		Matrix4f tempRot = Matrix4f::rotateVector(v, angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	/**
	* Adjust the pitch of the sphere by some amount
	* @param degrees Degrees to adjust pitch.
	*/
	void pitch(Vector3f position, GLfloat degrees) 
	{
		modelMat = modelMat * Matrix4f::translation(position.x, position.y, position.z);
		// modelMat = modelMat * Matrix4f::rotateRollPitchYaw(0.0, degrees, 0.0, true);
		// rotMat = rotMat * Matrix4f::rotateRollPitchYaw(0.0, degrees, 0.0, true);
		// modelMat = modelMat * Matrix4f::translation(-position.x, -position.y, -position.z);
	}

	/**
	* Draw the sphere using the supplied shaderProgram.
	* @param shaderProg Shader program to use.
	*/
	void draw(GLuint shaderProg) 
	{
		Matrix4f normalMat = Matrix4f::transpose(Matrix4f::inverse(this->rotMat));
		// this->translate(0, 0, 0.1);
		//modelMat = modelMat * Matrix4f::scale(size.x, size.y, size.z);


		int modelLoc = glGetUniformLocation(shaderProg, "modelMat");
		glUniformMatrix4fv(modelLoc, 1, 1, (float *)modelMat.vm);

		int normalMatLoc = glGetUniformLocation(shaderProg, "normalMat");
		glUniformMatrix4fv(normalMatLoc, 1, 1, (float *)normalMat.vm);

		int matAmbLoc = glGetUniformLocation(shaderProg, "materialAmb");
		glUniform4fv(matAmbLoc, 1, (float *)&materialAmbient);

		int matDiffLoc = glGetUniformLocation(shaderProg, "materialDiff");
		glUniform4fv(matDiffLoc, 1, (float *)&materialDiffuse);

		int matSpecLoc = glGetUniformLocation(shaderProg, "materialSpec");
		glUniform4fv(matSpecLoc, 1, (float *)&materialSpecular);

		int positionLoc = glGetAttribLocation(shaderProg, "vPosition");
		int normalLoc = glGetAttribLocation(shaderProg, "vNormal");
		glEnableVertexAttribArray(positionLoc);
		glEnableVertexAttribArray(normalLoc);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);

		// Tells OpenGL how to walk through the two VBOs
		struct cubeVertex v;
		int relAddress = (char *)v.pos - (char *)&v;
		glVertexAttribPointer(positionLoc, 4, GL_FLOAT, GL_FALSE, sizeof(struct cubeVertex), (char*)NULL + relAddress);
		relAddress = (char *)v.normal - (char *)&v;
		glVertexAttribPointer(normalLoc, 4, GL_FLOAT, GL_FALSE, sizeof(struct cubeVertex), (char*)NULL + relAddress);
		// draw the triangles
		glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_INT, (char*)NULL + 0);
		this->clear();
	}

	void setSize(Vector3f s) 
	{
		//this->size = s;
	}

	void scale(GLfloat xAmt, GLfloat yAmt, GLfloat zAmt) 
	{
		modelMat = modelMat * Matrix4f::scale(xAmt, yAmt, zAmt);
	}

	void translate(GLfloat x, GLfloat y, GLfloat z) 
	{
		modelMat = modelMat * Matrix4f::translation(x, y, z);
	}

	void setAmbient(GLfloat r, GLfloat g, GLfloat b) 
	{
		this->materialAmbient = Vector4f(r, g, b, 0.0);
	}

	void setDiffuse(GLfloat r, GLfloat g, GLfloat b) 
	{
		this->materialDiffuse = Vector4f(r, g, b, 0.0);
	}

	void setSpecular(GLfloat r, GLfloat g, GLfloat b) 
	{
		this->materialSpecular = Vector4f(r, g, b, 0.0);
	}

	void clear() 
	{
		this->modelMat = Matrix4f::identity();
		this->rotMat = Matrix4f::identity();
	}
};