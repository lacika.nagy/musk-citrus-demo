#pragma once
#include <glew.h>
#include <wglew.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <stdlib.h>
#include <vector>
#include <cmath>
#include "Vector.h"
#include "Matrix.h"

struct quadVertex
{
	float pos[4];
	float normal[4]; // the average normal
	float uv[2];
	short numFaces;  // number of faces shared by the vertex
	long colora;     // ambient colour - change to a colour structure
	long colord;     // diffuse color    - change to a colour structure
	long colors;     // specular colour  - change to a colour structure
};

class FullscreenQuad
{
protected:
	//Vector3f size;
	int numTriangles = 12;
	struct quadVertex * vtx = NULL;
	int sphereVBO;   // Vertex handle that contains interleaved positions and colors
	int triangleVBO; // Triangle handle that contains triangle indices
	int * ind = NULL;
	int numInd;
public:
	Matrix4f modelMat = Matrix4f::identity();
	Matrix4f rotMat = Matrix4f::identity();
	Vector4f materialAmbient;
	Vector4f materialDiffuse;
	Vector4f materialSpecular;

	FullscreenQuad()
	{
		materialAmbient = Vector4f(0, 0, 0, 0);
		materialDiffuse = Vector4f(0, 0, 0, 0);
		materialSpecular = Vector4f(0, 0, 0, 0);
		//this->size = size;

		int numVtx = 36;

		// allocate memory
		vtx = (struct quadVertex *) malloc(sizeof(struct quadVertex) * numVtx);
		if (vtx == NULL) {
			printf("Oops! An error occurred\n");

		}

		ind = (int *)malloc(sizeof(int) * numTriangles * 3);
		if (ind == NULL) {
			// error
			printf("Oops! An error occurred\n");
		}

		vtx[0].pos[0] = -1.0f;
		vtx[0].pos[1] = -1.0f;
		vtx[0].pos[2] = 0.0f;
		vtx[0].pos[3] = 1.0f;

		vtx[0].uv[0] = 0.0f;
		vtx[0].uv[1] = 0.0f;

		vtx[1].pos[0] = -1.0f;
		vtx[1].pos[1] = 1.0f;
		vtx[1].pos[2] = 0.0f;
		vtx[1].pos[3] = 1.0f;

		vtx[1].uv[0] = 0.0f;
		vtx[1].uv[1] = 1.0f;

		vtx[2].pos[0] = 1.0f;
		vtx[2].pos[1] = -1.0f;
		vtx[2].pos[2] = 0.0f;
		vtx[2].pos[3] = 1.0f;

		vtx[2].uv[0] = 1.0f;
		vtx[2].uv[1] = 0.0f;

		vtx[3].pos[0] = 1.0f;
		vtx[3].pos[1] = 1.0f;
		vtx[3].pos[2] = 0.0f;
		vtx[3].pos[3] = 1.0f;
		
		vtx[3].uv[0] = 1.0f;
		vtx[3].uv[1] = 1.0f;

		ind[0] = 1;
		ind[1] = 0;
		ind[2] = 2;

		ind[3] = 1;
		ind[4] = 2;
		ind[5] = 3;

		// *numVtx1 = numVtx;
		numInd = numTriangles * 3;

		glGenBuffers(1, (GLuint *)&sphereVBO);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(struct quadVertex)*numVtx, vtx, GL_STATIC_DRAW);
		glGenBuffers(1, (GLuint *)&triangleVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * numInd, ind, GL_STATIC_DRAW);
	}

	~FullscreenQuad()
	{
		std::cout << "Deleted sphere" << std::endl;
	}

	void rotateX(GLfloat angle, int degrees)
	{
		Matrix4f tempRot = Matrix4f::rotateX(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	Vector4f getPosition()
	{
		Vector4f pos = modelMat * Vector4f(0, 0, 0, 1.0);
		printf("x: %f, y: %f, z: %f\n", pos.x, pos.y, pos.z);
		return pos;
	}

	/**
	* Rotate the sphere around the y-axis.
	*
	* @param degrees Degrees to rotate it.
	*/
	void rotateY(GLfloat angle, int degrees)
	{
		Matrix4f tempRot = Matrix4f::rotateY(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	void rotateZ(GLfloat angle, int degrees)
	{
		Matrix4f tempRot = Matrix4f::rotateZ(angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	void rotateVector(Vector3f v, GLfloat angle, int degrees)
	{
		Matrix4f tempRot = Matrix4f::rotateVector(v, angle, degrees);
		rotMat = rotMat * tempRot;
		modelMat = modelMat * tempRot;
	}

	/**
	* Adjust the pitch of the sphere by some amount
	* @param degrees Degrees to adjust pitch.
	*/
	void pitch(Vector3f position, GLfloat degrees)
	{
		modelMat = modelMat * Matrix4f::translation(position.x, position.y, position.z);
		// modelMat = modelMat * Matrix4f::rotateRollPitchYaw(0.0, degrees, 0.0, true);
		// rotMat = rotMat * Matrix4f::rotateRollPitchYaw(0.0, degrees, 0.0, true);
		// modelMat = modelMat * Matrix4f::translation(-position.x, -position.y, -position.z);
	}

	/**
	* Draw the sphere using the supplied shaderProgram.
	* @param shaderProg Shader program to use.
	*/
	void draw(GLuint shaderProg)
	{
		//Matrix4f normalMat = Matrix4f::transpose(Matrix4f::inverse(this->rotMat));
		// this->translate(0, 0, 0.1);
		//modelMat = modelMat * Matrix4f::scale(size.x, size.y, size.z);

		int positionLoc = glGetAttribLocation(shaderProg, "vPosition");
		int normalLoc = glGetAttribLocation(shaderProg, "vNormal");
		int texloc = glGetAttribLocation(shaderProg, "vUv");
		glEnableVertexAttribArray(positionLoc);
		glEnableVertexAttribArray(normalLoc);
		glEnableVertexAttribArray(texloc);
		glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleVBO);

		// Tells OpenGL how to walk through the two VBOs
		struct quadVertex v;
		int relAddress = (char *)v.pos - (char *)&v;
		glVertexAttribPointer(positionLoc, 4, GL_FLOAT, GL_TRUE, sizeof(struct quadVertex), (char*)NULL + relAddress);
		relAddress = (char *)v.normal - (char *)&v;
		glVertexAttribPointer(normalLoc, 4, GL_FLOAT, GL_TRUE, sizeof(struct quadVertex), (char*)NULL + relAddress);
		relAddress = (char *)v.uv - (char *)&v;
		
		glVertexAttribPointer(texloc, 2, GL_FLOAT, GL_TRUE, sizeof(struct quadVertex), (char*)NULL + relAddress);
		// draw the triangles
		glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_INT, (char*)NULL + 0);
		this->clear();
	}

	void setSize(Vector3f s)
	{
		//this->size = s;
	}

	void scale(GLfloat xAmt, GLfloat yAmt, GLfloat zAmt)
	{
		modelMat = modelMat * Matrix4f::scale(xAmt, yAmt, zAmt);
	}

	void translate(GLfloat x, GLfloat y, GLfloat z)
	{
		modelMat = modelMat * Matrix4f::translation(x, y, z);
	}

	void setAmbient(GLfloat r, GLfloat g, GLfloat b)
	{
		this->materialAmbient = Vector4f(r, g, b, 0.0);
	}

	void setDiffuse(GLfloat r, GLfloat g, GLfloat b)
	{
		this->materialDiffuse = Vector4f(r, g, b, 0.0);
	}

	void setSpecular(GLfloat r, GLfloat g, GLfloat b)
	{
		this->materialSpecular = Vector4f(r, g, b, 0.0);
	}

	void clear()
	{
		this->modelMat = Matrix4f::identity();
		this->rotMat = Matrix4f::identity();
	}
};