#version 330

precision highp float;

out vec4 outColor;
in vec2 uv;

uniform sampler2D renderedTexture;

void main() 
{
	outColor = texture( renderedTexture, uv );
}