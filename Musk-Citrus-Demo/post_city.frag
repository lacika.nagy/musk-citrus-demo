#version 330

precision highp float;

out vec4 outColor;
in vec2 uv;
uniform vec2 resolution;
uniform sampler2D renderedTexture;
uniform float time;

vec2 barrelDistortion(vec2 coord, float amt) 
{
	vec2 cc = coord - 0.5;
	float dist = dot(cc, cc);
	return coord + cc * dist * amt;
}

float sat( float t )
{
	return clamp( t, 0.0, 1.0 );
}

float linterp( float t ) 
{
	return sat( 1.0 - abs( 2.0*t - 1.0 ) );
}

float remap( float t, float a, float b ) 
{
	return sat( (t - a) / (b - a) );
}

vec4 spectrum_offset( float t ) 
{
	vec4 ret;
	float lo = step(t,0.5);
	float hi = 1.0-lo;
	float w = linterp( remap( t, 1.0/6.0, 5.0/6.0 ) );
	ret = vec4(lo,1.0,hi, 1.) * vec4(1.0-w, w, 1.0-w, 1.);

	return pow( ret, vec4(1.0/2.2) );
}

const float max_distort = 0.4;
const int num_iter = 12;
const float reci_num_iter_f = 1.0 / float(num_iter);

vec4 color_correct(vec4 color)
{
	vec2 vuv=(gl_FragCoord.xy/resolution.xy*.5)-.25;
	color *=  1.0-length(vuv);
	float w = length(color.xyz);
	color = mix(color,vec4(w),w*1.5-.5);
	vec4 noise = vec4(sin(sin(w*51.0+gl_FragCoord.x*4112.184+gl_FragCoord.y*544.123)*613.0+w*834.2))*0.01;
	color -= vec4(.03,.01,.01,.0)*1.5;
	color += vec4(0.1/(1.0+time*8.0));
	return pow(color, vec4(1.0/1.9))+noise;
}

void main() 
{
	vec2 vuv=(gl_FragCoord.xy/resolution.xy);

	vec4 sumcol = vec4(mix(vec4(.00,.04,.09,1.0),vec4(.14,.1,.0,1.0),vuv.y)*2.0);
	vec4 sumw = vec4(0.0);	
	for ( int i=0; i<num_iter;++i )
	{
		float t = float(i) * reci_num_iter_f;
		vec4 w = spectrum_offset( t );
		sumw += w;
		sumcol += w * texture2D( renderedTexture, barrelDistortion(vuv, .6 * max_distort*t ) );
	}

	
	for ( int i=0; i<num_iter;++i )
	{
		float t = float(i) * reci_num_iter_f;
		vec4 w = spectrum_offset( t );
		sumw += w*.2;
		sumcol += max(vec4(.0),w * texture2D( renderedTexture, -.5*barrelDistortion(vuv, .6 * max_distort*t*4.0 )+vec2(.75) )*.2-vec4(.05));
	}
	outColor = color_correct(sumcol / sumw * 1.1);
}